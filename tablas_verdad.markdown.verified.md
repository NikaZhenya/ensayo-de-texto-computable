---
title: Ensayo de texto computable
'no': "\\sim"
"y": "\\cdot"
o: "\\lor"
ó: "\\oplus"
entonces: "\\implies"
solo_si: "\\iff"
toc:
  '1':
    tit: Un ensayo de lógica simbólica computable
  '1.1':
    tit: Declaración de las tablas de verdad
  1.1.1:
    tit: Tabla de identidad
  1.1.2:
    tit: Tabla de negación
  1.1.3:
    tit: Tabla de conjunción
  1.1.4:
    tit: Tabla de disyunción inclusiva
  1.1.5:
    tit: Tabla de disyunción exclusiva
  1.1.6:
    tit: Tabla de condición material
  1.1.7:
    tit: Tabla de bicondición
  '1.2':
    tit: Declaración de las proposiciones
  '1.3':
    tit: Declaración de la gramática
  1.3.1:
    tit: Gráfica de identidad
  1.3.2:
    tit: Gráfica de negación
  1.3.3:
    tit: Gráfica de conjunción
  1.3.4:
    tit: Gráfica de disyunción inclusiva
  1.3.5:
    tit: Gráfica de disyunción exclusiva
  1.3.6:
    tit: Gráfica de condición material
  1.3.7:
    tit: Gráfica de bicondición
  '1.4':
    tit: Programación de la gramática
  '1.5':
    tit: Pruebas de las gramática
  '2':
    tit: 'Actividad: ¿literatura computable?'
  '3':
    tit: Glosario de macros
  '3.1':
    tit: Primer ensayo
  '3.2':
    tit: Motor
  '3.3':
    tit: Archivo fuente
tags: ruweb, ensayo, ensayo computable, tabla de verdad, verdad
mods:
  joiner: ", "
  joiner_last: ", "
comunidades:
- "[Miau](https://miau.archlinux.mx)"
- "[Partido Interdimensional Pirata](https://partidopirata.com.ar)"
- "[Grafoscopio](https://t.me/grafoscopio)"
- "[Rancho Electrónico](https://ranchoelectronico.org)"
- "[Colima Hacklab](https://hacklab.cc)"
- "[4libertades](https://cuatrolibertades.org)"
- "[LIDSoL](https://t.me/lidsol)"
mundos_posibles:
- filosófico
- científico
- artístico
- económico
- político
- sociológico
- antropológico
- literario
- poético
- moderno
- contemporáneo
- cristiano
- musulmán
- capitalista
- comunista
disociaciones:
- un ejercicio analítico
- un deseo suicida
- un viaje alucinógeno
- un orgasmo
- un dolor del espíritu muy intenso
- un dolor del cuerpo insoportable
- una expresión artística.
verdad_falsedad:
- psicológica
- científica
- filosófica
- teológica
raimundo1: https://pad.programando.li/s/SkN7FSHoP#Maquinaria-de-Escritura-2-otra-m%C3%A1quina-de-Raimundo-Lulio
me2:
  link: https://gitlab.com/-/snippets/2066565
  img:
    pie: Figura 2. Mil muestras aleatorias de 61,120 publicaciones únicas producidas
      por ME-2 con una gramática de 72 palabras.
    link: https://i.imgur.com/pnz5Gie.png
venn:
  img:
    pie: Figura 1. La intersección entre $A$ y $B$ es el área en donde podemos emplear
      una gramática con $A \cdot B$.
    link: https://upload.wikimedia.org/wikipedia/commons/3/36/Diagrama_de_Venn_4b.png
knuth:
  wiki: https://es.wikipedia.org/wiki/Donald_Knuth
  nombre: Knuth
  nombre_completo: Donald Ervin Knuth
  analogía: 'Primero pensé que la programación era en principio análogo a la composición
    musical; la creación de patrones intrincados que están pensados para ser interpretados.
    Pero más tarde me di cuenta de una mejor analogía disponible: la programación
    es más comprensible como el proceso de creación de obras literarias que están
    hechas para ser leídas.'
  analogy: 'First, I thought programming was primarily analogous to musical composition—to
    the creation of intricate patterns, which are meant to be performed. But lately
    I have come to realize that a far better analogy is available: Programming is
    best regarded as the process of creating works of literature, which are meant
    to be read'
  entrada:
    tit: Why I Must Write Readable Programs
    link: https://web.archive.org/web/20170605163729/http://www.desy.de/user/projects/LitProg/Philosophy.html
    año: 1992
  proliteraria: https://pad.programando.li/ruweb#La-programaci%C3%B3n-literaria
  macros: https://pad.programando.li/s/ruweb#macro
  tex: https://es.wikipedia.org/wiki/TeX
  bio: "(10 de enero de 1938, Milwaukee, Wisconsin) es un reconocido experto en ciencias
    de la computación estadounidense y matemático, famoso por su fructífera investigación
    dentro del análisis de algoritmos y compiladores. Es profesor emérito de la Universidad
    de Stanford."
  foto: https://ieeecs-media.computer.org/wp-media/2018/03/11020301/donald-knuth-e1523412218270.jpg
homininos: https://es.wikipedia.org/wiki/Hominina
copi:
  nombre: Copi
  nombre_completo: Irving Marmer Copi
  wiki: https://es.wikipedia.org/wiki/Irving_Copi
  lógica: La lógica se ha definido con frecuencia como la ciencia del razonamiento.
    Esta definición, aunque da una clave a la naturaleza de la lógica, no es muy exacta.
    El razonamiento es la clase especial de pensamiento llamada inferencia, en la
    que se sacan conclusiones partiendo de premisas. Como pensamiento, sin embargo,
    no es campo exclusivo de la lógica, sino parte también de la materia de estudio
    del psicólogo. [El] lógico no se interesa en el proceso real del razonamiento.
    A él le importa la corrección del proceso completado.
  libro: Lógica simbólica
  libgen: http://libgen.rs/book/index.php?md5=243410617727EB1384D7EA5C31A9C8CA
  bio: "(28 de julio de 1917, Duluth, Minnesota – 19 de agosto de 2002, Honolulu,
    Hawái) fue un filósofo, lógico y estadunidense. Copi dictó clases en la Universidad
    de Illinois, en la Academia de la Fuerza Aérea de los Estados Unidos, en la Universidad
    de Princeton y en la Universidad de Georgetown, antes de terminar su carrera enseñando
    lógica en la Universidad de Míchigan desde 1958 hasta 1969. En sus últimos años
    Copi decidió alejarse y terminó su carrera dando clase en la Universidad de Hawái
    en Manoa desde 1969 hasta 1990."
  foto: https://www.ecured.cu/images/8/8f/Irving_Marmer_Copi.jpg
pineda:
  nombre_completo: Sebastián Pineda Buitrago
  wiki: https://es.wikipedia.org/wiki/Sebasti%C3%A1n_Pineda_Buitrago
prolibreros:
  nombre: Programando LIBREros
  link: https://programando.li/breros
discurso_metodo:
  tit: Discurso del método
  link: https://pad.programando.li/s/ruweb:pub:descartes
críticos:
  frondizi:
    nombre: Risieri Frondizi
    wiki: https://es.wikipedia.org/wiki/Risieri_Frondizi
  fernandez:
    nombre: Olga Fernández Prat
    link: https://www.uab.cat/web/el-departament/olga-fernandez-prat-1260171794484.html
tres_candados: https://www.youtube.com/watch?v=FQXLqdqtAbw
berdi:
  nombre: Berdiáyev
  wiki: https://es.wikipedia.org/wiki/Nikol%C3%A1i_Berdi%C3%A1yev
  creación: http://libgen.rs/book/index.php?md5=3CE6660565397C43A12797E14DE538ED
  creación_religiosa: en su esencia profunda, secreta, la creación, evidentemente,
    es religiosa
  ser_ellos_mismos: no atreviéndose a ser ellos mismos y nada más, los filósofos han
    intentado siempre imitar a los sabios, calcarse sobre ellos
wiki:
  knuth: https://es.wikipedia.org/wiki/Donald_Knuth
  copi: https://es.wikipedia.org/wiki/Irving_Copi
  peirce: https://es.wikipedia.org/wiki/Charles_Sanders_Peirce
  wittgenstein: https://es.wikipedia.org/wiki/Ludwig_Wittgenstein
  berdi: https://es.wikipedia.org/wiki/Nikol%C3%A1i_Berdi%C3%A1yev
  pineda: https://es.wikipedia.org/wiki/Sebasti%C3%A1n_Pineda_Buitrago
  frondizi: https://es.wikipedia.org/wiki/Risieri_Frondizi
  parsing: https://es.wikipedia.org/wiki/An%C3%A1lisis_sint%C3%A1ctico_(ling%C3%BC%C3%ADstica)
  ruby: https://es.wikipedia.org/wiki/Ruby
  cfg: https://es.wikipedia.org/wiki/Gram%C3%A1tica_libre_de_contexto
  goer: https://en.wikipedia.org/wiki/Ben_Goertzel
  capas: https://es.wikipedia.org/wiki/Capa_de_abstracci%C3%B3ns
  tablas: https://es.wikipedia.org/wiki/Tabla_de_verdad
  reglas: https://es.wikipedia.org/wiki/Regla_de_inferencia
  npl: https://es.wikipedia.org/wiki/Procesamiento_de_lenguajes_naturales
  beuchot: https://es.wikipedia.org/wiki/Mauricio_Beuchot
  pascal: https://es.wikipedia.org/wiki/Pascal_(lenguaje_de_programaci%C3%B3n)
  langC: https://es.wikipedia.org/wiki/C_(lenguaje_de_programaci%C3%B3n)
peirce:
  nombre_completo: Charles Sanders Peirce
  wiki: https://es.wikipedia.org/wiki/Charles_Sanders_Peirce
wittgenstein:
  nombre_completo: Ludwig Wittgenstein
  wiki: https://es.wikipedia.org/wiki/Ludwig_Wittgenstein
penrose:
  nombre_completo: Roger Penrose
  nombre: Penrose
  mente_nueva: http://libgen.rs/book/index.php?md5=6778B934754F51D0AD7E7724C6930662
  wiki: https://es.wikipedia.org/wiki/Roger_Penrose
  bio: "(8 de agosto de 1931, Colchester, Reino Unido) es un físico matemático británico
    y profesor emérito de la Universidad de Oxford. Es reconocido por su trabajo en
    física matemática, en particular por sus contribuciones a la teoría de la relatividad
    general y a la cosmología. También ha orientado sus esfuerzos en el ámbito de
    las matemáticas recreativas y es un polémico filósofo."
  foto: https://upload.wikimedia.org/wikipedia/commons/thumb/d/d5/Roger_Penrose_at_Festival_della_Scienza_Oct_29_2011.jpg/800px-Roger_Penrose_at_Festival_della_Scienza_Oct_29_2011.jpg
metadatos:
  link: https://pad.programando.li/zhenya:tablas-verdad?edit
  img:
    pie: Muestra de los metadatos de este texto, una ontología visible desde el panel
      de edición.
    link: https://i.imgur.com/jJM8iBB.png
creador_creado: http://maestria.perrotuerto.blog
copyleft: https://es.wikipedia.org/wiki/Copyleft
ruby: https://es.wikipedia.org/wiki/Ruby
cfg: https://es.wikipedia.org/wiki/Gram%C3%A1tica_libre_de_contexto
no_cfg: http://trevorjim.com/haskell-is-not-context-free
parsing: https://es.wikipedia.org/wiki/An%C3%A1lisis_sint%C3%A1ctico_(ling%C3%BC%C3%ADstica)
openai: https://openai.com
goer: https://en.wikipedia.org/wiki/Ben_Goertzel
capa: https://es.wikipedia.org/wiki/Capa_de_abstracci%C3%B3n
ruweb: https://pad.programando.li/ruweb
markdown: https://es.wikipedia.org/wiki/Markdown
hedgedoc: https://hedgedoc.org
G:
  T: true, false, \sim, \cdot, \lor, \oplus, \implies, \iff, (, )
  N: INI, VAL, LOG
  P:
    INI: VAL || LOG
    VAL: true || false
    LOG: "( \\sim INI ) || ( INI \\cdot INI ) || ( INI \\lor INI ) || ( INI \\oplus
      INI ) || ( INI \\implies INI ) || ( INI \\iff INI )"
  S: INI
O:
  atlanta_gana:
    str: Atlanta gana el campeonato de su división
    val: true
  baltimore_gana:
    str: Baltimore gana el campeonato de su división
    val: true
  chicago_gana:
    str: Chicago gana el Supertazón
    val: true
  dallas_gana1:
    str: Dallas gana el Supertazón
    val: true
  dallas_gana2:
    str: Dallas gana el Supertazón
    val: false
  estoy_explorando:
    str: una la estoy explorando
    val: true
  sé_poco:
    str: de la otra sé muy poco
    val: true
  estructuración_datos:
    str: la estructuración de datos
    val: true
  programación_computadoras:
    str: la programación de computadoras
    val: true
---

# Ensayo de texto computable

[Sebastián Pineda Buitrago](https://es.wikipedia.org/wiki/Sebasti%C3%A1n_Pineda_Buitrago) me pidió hacer una charla relacionada a la computación y la literatura. Pero una la estoy explorando y de la otra sé muy poco. Por fortuna, las comunidades FOSS[^1] me mostraron que existe un matemático y experto en ciencias de la computación, Donald Ervin Knuth, que de pasada habla de la analogía para la programación:

> Primero pensé que la programación era en principio análogo a la composición musical; la creación de patrones intrincados que están pensados para ser interpretados. Pero más tarde me di cuenta de una mejor analogía disponible: la programación es más comprensible como el proceso de creación de obras literarias que están hechas para ser leídas.[^2]

[^1]: FOSS es el acrónimo en inglés para _Free and Open Source Software_ (_software_ libre y de código abierto). En las comunidades FOSS encontré un espacio para aprender, hablar, reír y llorar. Algunas de estas comunidades son: [Miau](https://miau.archlinux.mx), [Partido Interdimensional Pirata](https://partidopirata.com.ar), [Grafoscopio](https://t.me/grafoscopio), [Rancho Electrónico](https://ranchoelectronico.org), [Colima Hacklab](https://hacklab.cc), [4libertades](https://cuatrolibertades.org), [LIDSoL](https://t.me/lidsol)...

[^2]: «First, I thought programming was primarily analogous to musical composition—to the creation of intricate patterns, which are meant to be performed. But lately I have come to realize that a far better analogy is available: Programming is best regarded as the process of creating works of literature, which are meant to be read», Knuth, «Why I Must Write Readable Programs», [_blog_ personal](https://web.archive.org/web/20170605163729/http://www.desy.de/user/projects/LitProg/Philosophy.html), 1992.

Knuth emplea la analogía para definir lo que es la programación. Así empieza a hablar sobre la «[programación literaria](https://pad.programando.li/ruweb#La-programaci%C3%B3n-literaria)», una manera de escribir código y documentación en el mismo documento. Esta escritura no es una mera intercalación entre bloques de texto ejecutables («código») y no ejecutables («prosa»). A partir de «[macros](https://pad.programando.li/s/ruweb#macro)», Knuth escribe y documenta código sin necesidad de seguir su orden de ejecución. Con esta técnica, la programación literaria se distingue por ser una manera de programar según un criterio discursivo.

:::info
<div class="bio">
  <div>
    <img src="https://ieeecs-media.computer.org/wp-media/2018/03/11020301/donald-knuth-e1523412218270.jpg" />
  </div>
  <div>

**Donald Ervin Knuth** (10 de enero de 1938, Milwaukee, Wisconsin) es un reconocido experto en ciencias de la computación estadounidense y matemático, famoso por su fructífera investigación dentro del análisis de algoritmos y compiladores. Es profesor emérito de la Universidad de Stanford.

  </div>
</div>

<a class="btn" href="https://es.wikipedia.org/wiki/Donald_Knuth">Continuar en Wikipedia</a>
:::

Esta forma de programar no es común, así como es frecuente que varias personas dedicadas a las ciencias computacionales la desconozcan. Es decir, esta manera de programar no es como se ha programado una infraestructura de telecomunicaciones ---como el internet---, un servidor, un sistema operativo, una «inteligencia artificial», un «_machine learning_», un bot, tu teléfono, tus _apps_, tus videos, fotos, texto, cara o voz... Esta manera de programar solo tiene un programa con mucha fama: [$\TeX$](https://es.wikipedia.org/wiki/TeX).

¿$\TeX$? Su «mucha fama» es relativa a las comunidades FOSS. En estas comunidades se considera un sistema de composición tipográfica superior al programa que usan la mayoría de los editores de libros. Su fama es tal que ha trascendido su extensión de archivo: es el motivo por el cual ves que «$\TeX$» está en una peculiar fuente itálica. En el archivo para este texto es posible usar sintaxis $\TeX$, aunque no sea este un archivo de puro $\TeX$.

Para Knuth, la programación literaria es la «cosa más importante que salió de $\TeX$». Para mí ha sido un camino para probar maneras de escribir textos computables, ensayos que ejecutan programas o experimentos que mezclan ideas para la estructuración de datos y la programación de computadoras. Estas nociones o ideas son cosas que me he imaginado a partir de mi formación en las humanidades y mis capacidades para jugar con la computación. Sin filosofía no estaría computando de esta manera; no obstante, sin computación no pensaría como lo hago ahora.

Por este motivo, aquí yace el sesgo que muestra lo poco que sé de literatura. Mi formación en las humanidades se ha enfocado más a la filosofía y su falta de accesibilidad. ¿Cómo hacer la filosofía más accesible para mí o para ti? ¿Cómo hacer la filosofía más accesible para quienes la editan, leen o escriben?

Entonces, ¿cómo hablar de la computación y la literatura? Lo intentaré por analogía a lo que vengo trabajando sobre la computación y la filosofía. Para ello se hace un ejercicio de lógica simbólica computable. De esta manera intento mostrar por dónde he trabajado la relación entre la filosofía y la computación. Para terminar, en conjunto pensaremos las analogías posibles entre la literatura y la computación.

## Un ensayo de lógica simbólica computable

Otra de las limitaciones de mi formación es que la lógica la conocí tarde. El profesor de lógica que tuve en la licenciatura en Filosofía ponía más empeño en mofarse de mis compañeros que en responder correctamente los ejercicios que nos daba. Por fortuna, el libro de cajón que usaba para la materia resultó ser la [_Lógica simbólica_](http://libgen.rs/book/index.php?md5=243410617727EB1384D7EA5C31A9C8CA) de Irving Marmer Copi.

En sus ~400 páginas, _Lógica simbólica_ es considerado uno de los mejores libros para aprender lógica. Después de mi licenciatura me dediqué a estudiar para enmendar algunas carencias en mi formación universitaria. En mis trabajos nocturnos en OXXO o en mis viajes a Guanajuato para visitar a Melisa[^3] fui completando las lecciones y ejercicios de Copi poco a poco.

[^3]: Mel y yo formamos parte de una pequeña unidad de trabajo, [Programando LIBREros](https://programando.li/breros), dedicada a ofrecer servicios editoriales, al desarrollo de _software_ libre para la edición y a la investigación sobre el cruce de la filosofía con la computación.

:::info
<div class="bio">
  <div>
    <img src="https://www.ecured.cu/images/8/8f/Irving_Marmer_Copi.jpg" />
  </div>
  <div>

**Irving Marmer Copi** (28 de julio de 1917, Duluth, Minnesota – 19 de agosto de 2002, Honolulu, Hawái) fue un filósofo, lógico y estadunidense. Copi dictó clases en la Universidad de Illinois, en la Academia de la Fuerza Aérea de los Estados Unidos, en la Universidad de Princeton y en la Universidad de Georgetown, antes de terminar su carrera enseñando lógica en la Universidad de Míchigan desde 1958 hasta 1969. En sus últimos años Copi decidió alejarse y terminó su carrera dando clase en la Universidad de Hawái en Manoa desde 1969 hasta 1990.

  </div>
</div>

<a class="btn" href="https://es.wikipedia.org/wiki/Irving_Copi">Continuar en Wikipedia</a>
:::

Por desgracia perdí la libreta que utilicé para responder los ejercicios de Copi. Sin embargo, el trabajo de formalización de la lógica que empezó en el siglo XIX, dando así comienzo a la «lógica simbólica», allanó el camino para su computabilidad y el ejercicio que haremos. Pero antes de continuar, déjame indicarte qué entiende Copi por «lógica»:

> La lógica se ha definido con frecuencia como la ciencia del razonamiento. Esta definición, aunque da una clave a la naturaleza de la lógica, no es muy exacta. El razonamiento es la clase especial de pensamiento llamada inferencia, en la que se sacan conclusiones partiendo de premisas. Como pensamiento, sin embargo, no es campo exclusivo de la lógica, sino parte también de la materia de estudio del psicólogo. [El] lógico no se interesa en el proceso real del razonamiento. A él le importa la corrección del proceso completado.

Que a la lógica no le competa el proceso _real_ del razonamiento me llevó a reflexionar dos cuestiones. Primero, el quehacer lógico dista mucho de una noción habitual de la lógica en la que se considera que esta «revela» o «corrobora» la realidad del mundo o de las cosas. En lugar de ello, las cosas que trabaja la lógica son las proposiciones u oraciones, y las maneras válidas o inválidas en como estas se relacionan. Para ser más explícito: el estudio de la realidad no es el objeto de la lógica, aunque, como Copi admite más adelante, tampoco se trata de una total independencia; hay una relación entre lo válido y lo verdadero, pero no es una cuestión que veremos ahora. Es decir, en la lógica se trabaja con ideas ---que al verbalizarse son ya proposiciones y, al escribirse, se hacen oraciones---, pero sin dar lugar a un idealismo; estas ideas, aunque sin relación directa con la realidad, no por ello implican un encuentro con lo divino ni un mundo de las ideas.

Este extraño lugar en el que acontecen las ideas tratadas por la lógica me permitió entender que esta no dictamina lo que es real o irreal: el mundo en el que vivimos no es completamente lógico o racional. De alguna manera hay puntos de fuga en donde no es posible encontrar significado o sentido, en donde la exigencia por su racionalización es una expresión del deseo (quizá hegeliano o cristiano) de equiparar lo real con la razón o la realidad con la _veritas_. Esta noción de la lógica me parece menos metafísica y teológica, así como más humilde, limitada y _ad hoc_ a nuestra condición y necesidades de nuestro tiempo.

La pertinencia para nuestros días de esta manera de entender la lógica me lleva a la segunda reflexión. En estos momentos tan graves para el mundo, donde cada vez más habitantes claman violencia y guerra, en una época donde al parecer se ha abierto la puerta negra de Mordor ---no confundir con la que tiene [tres candados](https://www.youtube.com/watch?v=FQXLqdqtAbw)--- para la batalla final entre los «hombres de Occidente» y los orcos de Sauron, me siento arrojado a expresar mi descontento. ¿Acaso eso es todo lo que podemos ser? ¿Un [hominino](https://es.wikipedia.org/wiki/Hominina) que vive en el ocaso del capitalismo occidental? ¿Un civil que la guerra me lleva a confiarle mi vida a un montón de varones armados? ¿Un sujeto que por angustia a ser sí mismo ha preferido ser un personaje literario de una obra épica de ficción medieval? ¿Un creyente en resistencia para que alguien no humano haga su aparición y, así, salvarnos? ¿Un hombre que no se atreve a realizar su autocrítica radical y en su lugar se esfuerza en criticar o cancelar a otros?

<figure>
    
  {%youtube Wly-NbEt7Ro %}

  <figcaption>La batalla final del hombre occidental con un bello doblaje en castellano, c. finales del segundo milenio de la Tercera Edad del Sol.</figcaption>
</figure>

Mi descontento es una exigencia para mí y para ti ---si es que esto te interpela---. Esta exigencia es verme como supuesto sujeto decolonial o «deconstruido» al mismo tiempo que en mi discurso me valgo de metáforas cristianas o modernas para justificarlo y, en su camino, edificarme. Con esto no quiero decir que la decolonialidad o la deconstruicción de manera forzosa _deban estar_ alejadas del cristianismo o la modernidad. No es así como Derrida pensó la deconstrucción. No es con una mera negación o reinterpretación de nuestro pasado como podremos «liberarnos» de la colonización.

<figure>
    
  {%youtube 2dFM1OO315k %}

  <figcaption>
    
_Por otra parte, Jacques Derrida_, documental autobiográfico en el que Derrida habla, entre otras cosas, de su colonialidad.

  </figcaption>
</figure>

A lo que hago referencia es a una pregunta que me ha puesto a releer el [_Discurso del método_](https://pad.programando.li/s/ruweb:pub:descartes) y a revisar de nuevo los ejercicios de Copi: ¿es posible _hackear_[^4] uno de los grandes logros de la modernidad; a saber, la posición privilegiada en la que se coloca el sujeto que habla en su propio discurso?

[^4]: Incluso en el contexto de las nuevas tecnologías de la información y la comunicación este anglisismo es polisémico. A las comunidades FOSS les debo la adopción de este término para mi preguntar. De los distintos sentidos, el que más me ha interesado para [mi praxis](http://maestria.perrotuerto.blog) es el _hackeo_ del [_copyleft_](https://es.wikipedia.org/wiki/Copyleft). En sus orígenes el derecho de autor abarcaba 15 años después de la publicación de la obra, además de requerir un registro para su protección. En la actualidad se extiende hasta 100 años después de la muerte del autor, así como no necesita un registro para su aplicación. Ante esta maximización del derecho de autor, el _copyleft_ ---el término pretende explicitar su contraposición al _copyright_--- es un implemento legal elaborado en ~1986 por un grupo de progamadores del MIT por el cual el autor consiente en que su obra pueda ser utilizada por otras personas bajo una licencia no exclusiva que garantiza sus libertades de uso, estudio, modificación y distribución sin ninguna garantía. Esto quiere decir que en un sentido legislativo el _copyleft_ no va en contra del _copyright_. En su lugar, se trata de un traslado de un ámbito de «todos los derechos reservados» a «algunos derechos reservados» ---como el derecho a recibir atribución, si la licencia así lo estipula---. Este traslado es reconocido por diversas legislaciones como en la Ley Federal del Derecho de Autor del Estado mexicano. Como persona que hace trabajo intelectual, por ley puedes exigir que tu obra sea publicada con alguna licencia _copyleft_. El _hack_ del _copyleft_ es doble. Por un lado flexibiliza una ley cuya pujante rigidez está impidiendo la producción de obras literarias ---lo que se dice proteger y fomentar---. Por el otro, para que una licencia de uso se considere _copyleft_ se requiere de una cláusula que evite su reapropiación privada: cuando publicas con _copyleft_ estipulas que cualquier obra derivada de tu trabajo debe también estar liberada con una licencia _copyleft_. El _hack_ es esta capacidad de revertir las intenciones de privatización y centralización de una ley a favor de un ecosistema de bienes intelectuales comunitario y descentralizado ---no tienes que pedirme permiso explícito para usar mi trabajo, ni es necesaria una entidad que avale tu uso cada vez que quieras hacerlo---. Su traslado semántico al _hackeo_ de los logros de la modernidad lo entiendo como una búsqueda por flexibilizar o revertir los ideales de la modernidad en pos de los ideales de nuestra contemporaneidad. Este otro sentido del _hack_ no es elaboración mía ni me parece que se trate de un «salto» de un campo a otro. La historia del derecho de autor es una muestra de la paulatina modernización ---o invención--- del autor como sujeto jurídico a través de la elaboración del derecho positivo para las obras literarias. El derecho de autor es un triunfo de la modernidad porque despojó a la «creación» de su derecho divino y en su lugar la estableció en un punto de fuga que se localiza dentro del ámbito privado y erótico del autor y su obra. El _copyleft_ se presentaría a finales del siglo XX como aquello que flexibiliza o revierte el programa moderno del derecho de autor de ser para sí en el erotismo de su propia habitación. Es decir, para mí el _copyleft_ es una evidencia de la posibilidad de _hackear_ algunos logros de la modernidad. Mi deseo entonces no consiste en extrapolar un término, sino en explorar y profundizar sus posibles consecuencias.

Descartes y sus críticos, como [Risieri Frondizi](https://es.wikipedia.org/wiki/Risieri_Frondizi) y [Olga Fernández Prat](https://www.uab.cat/web/el-departament/olga-fernandez-prat-1260171794484.html), nos dan caminos para pensar al sujeto cartesiano. En cuanto la posición respecto a su método, hay un consentimiento en que Descartes escribe con supuesta humildad un discurso que de antemano sabía que provocaría más de una reacción entre los filósofos de su tiempo. Con ello considero que tengo una muestra de que la falsa humildad en mi discurso no sería suficiente para «descolocarme» de la posición privilegiada en la que me pongo cuando hablo: es un artificio que el padre de la filosofía moderna llevó acabo hace varios siglos. Respecto a la actitud de Descartes sobre su obra, estas personas mencionan que, cuando se enteró de las consecuencias que afrontó Galileo por su _Mensajero sideral_, decidió no publicar su tratado de _El mundo_; esta obra se editaría después de su muerte.

Hay, desde los comienzos de la filosofía moderna, una especie de temor por el cual el «_eppur si muove_» opacó al «_cogito ergo sum_» hasta el punto donde hoy en día varias personas dedicadas a la ciencias, como algunas que trabajan en las ciencias de la computación, tratan con desdén y profundo escepticismo todo aquello que esté tildado de «filosofía», sin importar que la lógica por la cual dependen sea un trabajo de personalidades que diambulan en diversas historias de la filosofía. Un miedo que siglos después Husserl olería para motivarlo a «desarrollar radicalmente motivos cartesianos» en sus meditaciones. Un pavor que, en [_El sentido de la creación_](http://libgen.rs/book/index.php?md5=3CE6660565397C43A12797E14DE538ED), [Berdiáyev](https://es.wikipedia.org/wiki/Nikol%C3%A1i_Berdi%C3%A1yev) cataloga como «cientiforme»; es decir, una especie de envidia a las ciencias y de servidumbre a la teología.[^6] Para mi exigencia, el temor que siento se me presenta como la imposibilidad de dejar de lado mi preguntar ---es decir, mi filosofar---: no basta con ser cobarde o con guardar silencio para «despojar» a mi yo de la posición privilegiada por la que discurre.

[^6]: Esta obra de Berdiáyev es muy sugestiva para quienes sospechamos de la «crisis» de la filosofía. Entre los pasillos y las aulas de las escuelas y facultades dedicadas a esta disciplina, durante al menos un siglo se ha hablado de su crisis, de su muerte, de su haber sido y que ya jamás volverá a ser: de su ocaso y despedida. ¿Qué clase de crisis es esta que llama más a la nostalgia por un pasado renombrable que a una voluntad por un futuro menos quejoso? Tal vez huele, de nuevo, a hombres «cientiformes» que, para Berdiáyev, «no atreviéndose a ser ellos mismos y nada más, los filósofos han intentado siempre imitar a los sabios, calcarse sobre ellos». Aunque sugestivo, hay una cuestión que me deja intranquilo. En su búsqueda por una filosofía con «espíritu autónomo», Berdiáyev nos termina por llevar a un creacionismo. Los problemas con la «creación» son múltiples, pero los más graves para los propósitos de Berdiáyev son al menos dos. Primero, de manera acertada Berdiáyev explica que la creación antecede a la filosofía: hay creación en la filosofía, pero también hay creación de otra índole por la que es irreducible a un quehacer filosófico; por ejemplo, la creación artística. ¿No es acaso esto una confesión de que la filosofía, para ser ella misma, termina por recaer en algo distinto? Aquí podría argumentarse que, como la creación implica a la filosofía y más, la filosofía no deja de ser ella misma si reposa sobre su capacidad creativa. Sin embargo, me parece que hay una segunda dificultad. Para Berdiáyev la irreducibilidad del acto creativo reside en que «en su esencia profunda, secreta, la creación, evidentemente, es religiosa». En su crítica del carácter cientiforme de la filosofía, de su envidia a las ciencias y de su servidumbre a la teología, Berdiáyev nos lleva a una filosofía que de manera activa busca encontrarse con un «Cristo Futuro». La filosofía, de nueva cuenta, termina por arrollidarse ante un gran otro. Entonces, para mí la importancia de Berdiáyev es su valentía en defender que no existe crisis en la filosofía más allá del temor de los filósofos a ser ellos mismos. Sin embargo, su respuesta me lleva a tomar distancia debido a que me provoca una nueva duda ---es decir, con sus argumentos Berdiáyev activa, a su pesar, mi sujeto cartesiano---: ¿alguna vez la filosofía ha sido o podrá ser ella misma?

En esta búsqueda la noción de la lógica de Copi, para mí «secularizada», me abre un campo de posibilidades. Quizá no pueda «abandonar» esa posición de privilegio en mi discurso por la cual tengo la libertad de elegir argumentos, elaborar artificios retóricos, incrustar sesgos de manera intencional o inconsciente, además de decidir sobre lo que he de hablar o callar. Pero sin dudas me es posible limitar las pretensiones de mi discurso si de alguna manera te dejo expuesto el aparato lógico y óntico ([ver metadatos](https://pad.programando.li/zhenya:tablas-verdad?edit)) con el que procuro justificar y validar mis aseveraciones. Gracias a la posibilidad de computar este texto me es posible dejarte más huellas para que mi autocrítica la uses para tu crítica.

<figure>
    
![](https://i.imgur.com/jJM8iBB.png)

  <figcaption>Muestra de los metadatos de este texto, una ontología visible desde el panel de edición.</figcaption>
</figure>

Entonces, para este primer ejercicio ensayaré de manera limitada la posibilidad de entregarles el aparato lógico de este discurso. La limitación se debe a que, de la lógica simbólica, solo programé la formalización de las «[tablas de verdad](https://es.wikipedia.org/wiki/Tabla_de_verdad)». Estas tablas son un punto de partida de una aventura que, para su profunda vivencia, requeriría en otras ocasiones la computación de las [reglas de inferencia](https://es.wikipedia.org/wiki/Regla_de_inferencia) y cuestiones de lógica de primer o segundo orden.

Pese a esta limitación, es importante señalar que con el entendimiento de las tablas de verdad contarías con la lógica necesaria para aprender a programar en cualquier lenguaje de tu preferencia. Aunque si tomas esto con estusiasmo, siento la obligación de advertirte que, en mis andanzas, los epiciclos[^5] presentes en la estructura de datos de los lenguajes de programación actuales ---elementos de su teoría geocéntrica que no termino de comprender--- son más difíciles de aprehender que su lógica computacional o su idioma imperial.

[^5]: No lo digo con desdén. Los epiciclos fueron parches a una teoría para corresponder con los datos obtenidos. Esto hizo más compleja la explicación de los movimientos celestes, pero fueron operativos desde la Antigua Grecia hasta el Renacimiento. Los epiciclos se retiraron cuando Copérnico dio una explicación matemática al heliocentrismo, una idea al menos tan antigua como el geocentrismo. Los epiciclos funcionaron para mantener viva una teoría equívoca, estos no eran el problema, sino la insospechada y errónea manera en como se entendía la astronomía. Entonces, por «epiciclos» en los lenguajes de programación actuales y, con ello, su equiparación con una «teoría geocéntrica», hago referencia a que quizá un día la programación, en la realidad y en la ficción, deje de representarse como un texto en inglés encapsulado por llaves, paréntesis, espacios o `end`, con un montón de puntuación, y con un orden rígido y repetitivo. En el futuro el código podría ser otra cosa, incluso dejar de ser texto o volverse innecesario para la programación. Aunque como amante de la lectura no puedo negar el sesgo de imaginarme que, si el texto computable está pensado para ser leído, entonces un día las técnicas que facilitan su escritura y lectura podrían matizar los reinos del «código» y de la «prosa», o dar con un nuevo reino.

### Declaración de las tablas de verdad

Las tablas de verdad permiten asignar valores de «verdadero» o «falso» a las proposiciones. Pero como te mencioné, la lógica no trata la noción habitual de «verdad», sino de «validez». Estas tablas de verdad fueron formalizadas por [Charles Sanders Peirce](https://es.wikipedia.org/wiki/Charles_Sanders_Peirce) y [Ludwig Wittgenstein](https://es.wikipedia.org/wiki/Ludwig_Wittgenstein). Aquí obviaremos esta interesante historia para irnos de lleno al meollo _no exhaustivo_ de estas tablas.

#### Tabla de identidad

Todo $V$ es siempre $V$ y todo $F$, $F$:

|$p$|$p$|
|---|---|
|$V$|$V$|
|$F$|$F$|

Ahora traduzco esta declaración en un lenguaje de programación. El lenguaje de mi preferencia es [Ruby](https://es.wikipedia.org/wiki/Ruby):

```ruby _identidad
p
```

:::warning
¿Por qué Ruby? Esta elección se debe a los siguientes motivos:

* biográficos, en mi localidad hay una comunidad pujante de Ruby, me convenció la pasión con la que hablan sobre Ruby
* lingüísticos, este lenguaje es una [gramática libre de contexto](https://es.wikipedia.org/wiki/Gram%C3%A1tica_libre_de_contexto) que evita técnicas _ad hoc_ para su [análisis sintáctico](https://es.wikipedia.org/wiki/An%C3%A1lisis_sint%C3%A1ctico_(ling%C3%BC%C3%ADstica)) como sucede con [Python o Haskell](http://trevorjim.com/haskell-is-not-context-free)
* editorial, me parece un lenguaje menos accidentado para una experiencia de lectura

Sin embargo, esto no hace de Ruby un mejor lenguaje. Por ejemplo, es famoso por ser «lento». Lo importante no es que elijas el mejor lenguaje de programación, sino que escojas el que te dé mayor comodidad o, si quieres vivir del _coding_, el que te haga ganar más dinero, aunque pases el resto de tus días detrás de una computadora...
:::

#### Tabla de negación

Todo $\sim V$ es $F$ y visceversa:

|$p$|$\sim p$|
|---|---|
|$V$|$F$|
|$F$|$V$|

De nuevo codificamos:

```ruby _contradicción
not p
```

#### Tabla de conjunción

Si alguno es $F$, el resultado es siempre $F$:

|$p$|$q$|$p \cdot q$|
|---|---|---|
|$V$|$V$|$V$|
|$F$|$F$|$F$|
|$V$|$F$|$F$|
|$F$|$V$|$F$|

Codificado:

```ruby _conjunción
p and q
```

#### Tabla de disyunción inclusiva

Si alguno es $V$, el resultado es siempre $V$:

|$p$|$q$|$p \lor q$|
|---|---|---|
|$F$|$F$|$F$|
|$V$|$V$|$V$|
|$V$|$F$|$V$|
|$F$|$V$|$V$|

Codificado:

```ruby _disyunción_inclusiva
p or q
```

#### Tabla de disyunción exclusiva

Si solo uno es $V$, el resultado es siempre $V$:

|$p$|$q$|$p \oplus q$|
|---|---|---|
|$F$|$F$|$F$|
|$V$|$V$|$F$|
|$V$|$F$|$V$|
|$F$|$V$|$V$|

Codificado:

```ruby _disyunción_exclusiva
(p or q) and not(p and q)
```

Hasta aquí la codificación está más o menos clara:

* `===` simboliza identidad
* `!`, negación
* `&&`, conjunción
* `||`, disyunción

En una disyunción exclusiva se busca el resultado de una disyunción ($p \lor q$) pero en la que ambos elementos no tienen el mismo valor, por eso se codificó como $( p \lor q ) \cdot \sim ( p \cdot q )$.

#### Tabla de condición material

Si $V \implies F$, el resultado es siempre $F$:

|$p$|$q$|$p \implies q$|
|---|---|---|
|$F$|$F$|$V$|
|$V$|$V$|$V$|
|$V$|$F$|$F$|
|$F$|$V$|$V$|

Codificado:

```ruby _condición_material
not(p and not q)
```

Con esta codificación se indica que no puede $p$ ser $V$ y $q$, $F$. La condición material sería una manera de abreviar $\sim ( p \cdot \sim q )$ por un simple $p \implies q$.

#### Tabla de bicondición

Si y solo si ambos son $F$ ó $V$, el resultado es siempre $V$:

|$p$|$q$|$p \iff q$|
|---|---|---|
|$F$|$F$|$V$|
|$V$|$V$|$V$|
|$V$|$F$|$F$|
|$F$|$V$|$F$|

Codificado:

```ruby _bicondición
not(p and not q) and not(q and not p)
```

El bicondicional también se comporta como una abreviatura. En este caso sintetiza $p \implies q \cdot q \implies p$ por un llano $p \iff q$.

Esta es la última tabla que declaramos por ahora. Existen otras que no fueron declaradas pero que funcionan a modo de abreviaturas para un juego entre identidades, negaciones, conjunciones o disyunciones. Es decir, la lógica simbólica es una lógica muy potente porque permite validar una gran cantidad de elementos y sus relaciones con una pequeña «gramática». En este contexto por «gramática» se hace referencia a una formalización de valores y sus reglas de relación. Sin embargo, esto también evidencia su carácter limitado: sin gramática no hay lógica que podamos usar para suicidarme, inmolarnos o destruir a nuestros adversarios.

Para mí esta limitación no es una desventaja, sino la explicitación de aquello que te comentaba al comienzo: la lógica no está fundada sobre un gran otro, se ha secularizado; al mismo tiempo demuestra su incapacidad de reducir lo real a lo racional o la realidad a la verdad. ¿Por qué lo percibo así? Debido a que, de lo contrario, estaríamos confesando que en lo real o la realidad existe de suyo una «gramática del mundo» por descubrir, cuando en nuestros días las gramáticas se consideran construcciones sociolingüísticas, mundos que más bien construimos.

Tal vez te pueda provocar desdicha saber que nuestro «buen sentido» ---palabra que usa Descartes para referirse a la razón, la imaginación, las afecciones y todo aquello que sentimos o dilucidamos cuando pensamos--- no es suficiente para entenderlo todo, para reducir los mundos en un mundo filosófico, científico, artístico, económico, político, sociológico, antropológico, literario, poético, moderno, contemporáneo, cristiano, musulmán, capitalista, comunista; es decir, para hacer del mundo una gramática adecuada a nuestro _buen_ sentido. Más desventura podría causarte caer en cuenta que semejante impedimento hace de nuestras verdades, razones y sentimientos algo efímero, un calzado que mudamos con el tiempo. El desasosiego es muy grande y quizá te lleve al suicidio: ¿acaso no tenemos raíz en esta Tierra?

No obstante, por eso te he insistido a no tener miedo a pensar por nosotros mismos. Si pensar en sí mismo es un acto que desde su comienzo anticipa su limitación, su sujeción a las gramáticas a las que recurrimos para pensarnos, entonces quiere decir que existe un punto de fuga, aquello que en el éxtasis por nombrarlo, nos lleva a acuñar más y más nombres sin nunca alcanzarlo.[^7][^8] No me parece que se trate de un hoyo negro que fagocita a nuestro mundo y, con ello, que lleva a nuestro pensamiento a su aniquilación, ¡qué alivio sería: la garantía de que podemos hacer sin pensar; la justificación para dejar de pensar y, según, vivir!

Su movimiento lo percibo como una delimitación mas no una reducción del mundo en el que vivimos. Así que prefiero imaginar ese punto de fuga como el horizonte o límite que de manera incesante nos vomita más elementos para nuestras gramáticas, para hacernos a nosotros mismos. De ser así, alégrate, ese punto de fuga nos escupe a cada instante un torrente de significaciones y sentidos, ¿tendremos el coraje de darles cobijo, de incorporarlos a nuestra visión contemporánea del mundo y de nosotros mismos?

[^7]: Por esto me aferro a pensar que no puede eso ser alguna clase de dios, además de no garantizar la transustanciación de la subjetividad en una mente sin cuerpo, la garantía de una vida después de la muerte. Como Descartes empiezo a concebir el acto de pensar como el ejercicio del buen sentido: pienso, luego siento; siento, luego pienso. Es decir, me siento incapaz de disociar mi vivencia del pensar más allá de un ejercicio analítico, un deseo suicida, un viaje alucinógeno, un orgasmo, un dolor del espíritu muy intenso, un dolor del cuerpo insoportable, una expresión artística.. Cuando pienso me abstraigo y me reflejo pero también me asombro, río, deprimo, angustio o lloro; se me pone la piel de gallina y siento que me ahogo mientras caigo en un hoyo negro. Por ello para mí y con el establecimiento de una relación de Berdiáyev con Descartes, pensar a dios es ya un ejercicio del buen sentido, sea para derivarlo en una vivencia mística o para racionalizarlo en un tratado de teología o metafísica. Si pretendo encontrar a un dios detrás de todo lo vivo, a hacer del punto de fuga el motor de todo lo que ha existido, existe y existirá, estaría pensando hacia las causas a partir de los efectos. Es decir, de manera racional o mística me retraería hasta encontrar nombres sin cuerpo: poco a poco haría de mi búsqueda una construcción de palabras o conceptos que manifiestan mi pensar y mi sentir.

[^8]: Esto realiza una referencia a Aquíles y la tortuga. Aquíles nunca logra alcanzar a la tortuga porque cada vez que se acerca se queda solo un pelín más cerca. Hasta el infinito Aquíles corre detrás de la tortuga. Esta paradoja es un pensamiento presocrático, se le atribuye a Zenón de Elea. El artificio reside en suponer que Aquíles corre sobre la Tierra como si hiciera un cálculo infinitesimal. En este supuesto, cada paso que da Aquíles, por más grande que sea, termina por ser, por ejemplo, la mitad recorrida en el paso anterior. Doy un paso y recorro 50 cm. Doy otro paso, uno largo, y solo consigo avanzar 25 cm. Me desespero y ahora doy un salto como Michael Jordan... para mi desgracia descubro que solo salté 12.5 cm, ¿seguiré saltando? Aquello por nombrarse avanza al ritmo de un cálculo infinitesimal, un producto de nuestra mente.

### Declaración de las proposiciones

Con la declaración de las tablas de verdad cuento con reglas de relaciones válidas o inválidas entre elementos. En la lógica simbólica, estos elementos solo pueden tener uno de dos valores de verdad: son falsos ó verdaderos. Sin embargo, pese a haber declarado sus relaciones y suponer sus dos posibles valores, aún no los he declarado de manera formal.

¿Cuáles son estos elementos? Se trata de las proposiciones que al enunciarse son también oraciones. Vale la pena insistirte que las proposiciones, esas objetos que estudia la lógica, no son cosas que están de suyo en el mundo ni cosas que hacemos con materia. Debido a que la lógica, como nos indica Copi, no trata de lo real, sino que en su lugar es un proceso posterior e intelectual de validación de lo ya hecho, las proposiciones no son algo que esté ahí afuera que puedas encontrar o descubrir. Las proposiciones son cosas que forjas cuando de algún tipo de verdad o falsedad psicológica, científica, filosófica, teológica... las articulas entre sí para obtener conclusiones. Las proposiciones son un conjunto de «ideas» dispuestas con ciertas reglas para discurrir sobre dioses, mundos u hombres.

Por lo regular las proposiciones las declaramos sin caer en cuenta, sin darle mucha importancia. En cada afirmación o negación que llevas a cabo hay una o más proposiciones. Por ejemplo, en paráfrasis a Copi tenemos estas proposiciones:

```
Todos los homininos van a morir
Tú eres un hominino
Por lo tanto, tú vas a morir
```

Aquí tenemos tres proposiciones. Las primeras dos son las premisas para dar con la tercera proposición, que en este argumento funciona a modo de conclusión. Un conjunto ordenado ---con reglas de operación--- de proposiciones hacen un argumento. Las proposiciones acomodadas en un argumento se constituyen como premisas o conclusiones. Con esto tenemos relaciones en torno a las proposiciones, argumentos, premisas y conclusiones.

Sin embargo, esta manera «natural» con la que expresamos proposiciones no nos permite su computabilidad. En los lenguajes de programación de la actualidad el lenguaje natural es un problema que de diversas maneras se está buscando «procesar». En las ciencias computacionales esto se conoce como [procesamiento de lenguajes naturales](https://es.wikipedia.org/wiki/Procesamiento_de_lenguajes_naturales) (NPL, por sus siglas en inglés). De las aproximaciones existentes, las más conocidas son las que se aglutinan con los términos de «inteligencia artificial» o «_machine learning_». De los proyectos más interesantes en este sentido está [OpenAI](https://openai.com).

Este tipo de aproximaciones basadas en inteligencia artificial tienen la ventaja ante un gran problema, ¿cómo procesar todos los textos que nuestra especie ha escrito? Si le asignáramos la tarea a un grupo de personas, su capacidad para reeditar textos para su procesamiento nos llevaría a esperar décadas o siglos.

Sin embargo, el uso de la inteligencia artificial para responder a esta necesidad de procesamiento de grandes volúmenes de información nos abre otras dificultades que no son reducibles a un problema técnico o de computación. Por ejemplo, uno de los primeros problemas es el mismo término «inteligencia artificial». Para la mayoría de las personas esta palabra les hace pensar en un superhombre sin cuerpo, en una máquina con capacidades intelectuales sobrenaturales. Esta noción, en mi opinión, tiene su fuente en al menos dos fenómenos: la retroalimentación entre la realidad y la ficción que es observable en la ciencia ficción ---varios de los exponentes de la inteligencia artificial son también consumidores insaciables de este género literario---, y el optimismo de quienes se dedican a este campo que les hace pensar en la posibilidad de constitución de un superhombre ---¿cuántas veces [Ben Goertzel](https://en.wikipedia.org/wiki/Ben_Goertzel) no nos ha hablado del segundo despertar, es decir, de la singularidad de la inteligencia artificial?---.

<figure>
    
  {%youtube ltDd8Ukh9KA %}

  <figcaption>Ben Goertzel otra vez nos afirma que entre 2025-45 los homininos «crearán una inteligencia artificial tan inteligente como un ser humano», c. 2019.</figcaption>
</figure>

Por fortuna, desde una etapa muy temprana de la gestación de la inteligencia artificial han existido críticos ante este problema, como Roger Penrose. En [_La mente nueva del emperador. En torno a la cibernetica, la mente y las leyes de la fisica_](http://libgen.rs/book/index.php?md5=6778B934754F51D0AD7E7724C6930662), Penrose es escéptico ante la posibilidad de una «inteligencia artificial». Su argumento comienza con un desmantelamiento del término a sus elementos constitutivos: la programación de algoritmos. La cuestión, nos dice Penrose, es que los algoritmos son efectivos en una parcela limitada de la realidad y, por lo tanto, de lo que pensamos e inteligimos. Para que un algoritmo funcione no solo requiere un conjunto de reglas, sino también de finitud. Además, si bien los algoritmos se declaran matemáticamente, estos no son capaces de computar todos los números.

Aquí es donde Penrose lanza un desafío: si hay números que no pueden computarse y, por ende, son incapaces de «algoritmizarse»; si además de esta limitación de aplicación matemática de los algoritmos, nuestro pensar se desborda hacia el infinito y, por ello, existen pensamientos irreducibles a algoritmos, ¿qué nos hace pensar que el mundo es reducible a una de sus facetas; a saber, el mundo algorítmico de la inteligencia artificial?

:::info
<div class="bio">
  <div>
    <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/d/d5/Roger_Penrose_at_Festival_della_Scienza_Oct_29_2011.jpg/800px-Roger_Penrose_at_Festival_della_Scienza_Oct_29_2011.jpg" />
  </div>
  <div>

**Roger Penrose** (8 de agosto de 1931, Colchester, Reino Unido) es un físico matemático británico y profesor emérito de la Universidad de Oxford. Es reconocido por su trabajo en física matemática, en particular por sus contribuciones a la teoría de la relatividad general y a la cosmología. También ha orientado sus esfuerzos en el ámbito de las matemáticas recreativas y es un polémico filósofo.

  </div>
</div>

<a class="btn" href="https://es.wikipedia.org/wiki/Roger_Penrose">Continuar en Wikipedia</a>
:::

En las comunidades en las que habito esta pretensión se reformula en términos más sencillos: la inteligencia artificial y el _machine learning_ son palabrejas para hablar de manera velada sobre cálculos matemáticos y, dado a que la vida no es reducible a las mates, la «inteligencia» más bien denota un ámbito de aplicación muy reducido. Con esto podemos indicar otros problemas de esta aproximación.

En párrafos anteriores te mencioné sobre la _necesidad_ de procesar los textos que nuestra especie ha elaborado. ¿Por qué, cuando nos percatamos de una nueva dimensión para los textos ---su computabilidad---, de repente surge un sentimiento de urgencia que nos lleva al deseo de computarlos todos?

Existen motivos intelectuales que me parecen valiosos. Por ejemplo, la computabilidad de los textos es una dimensión adicional a lo que veníamos entendiendo por «texto». Al ser una dimensión que recién estamos explorando, de repente, aquellos textos que al parecer nos decían poco más allá de una de las mayores preocupaciones de [Mauricio Beuchot](https://es.wikipedia.org/wiki/Mauricio_Beuchot) ---el infame equivocismo hermenéutico---, ahora habitan sobre un nuevo _espacio_ en cuya reflexión es posible una «hermenéutica computacional», una manera de interpretar los textos a partir de los resultados de su cómputo.

Sin embargo, como se trata de un valor adicional de los objetos que a través del derecho de autor y de patentes se mercantilizan como propiedad intelectual o, mediante el secreto industrial, permiten una mayor competitividad, las millonarias inversiones a la inteligencia artificial por parte de corporaciones o instituciones estatales no son fortuitas. La inteligencia artificial está abriendo posibilidades económicas, sociales, políticas y, por supuesto, militares que, quienes sean los primeros en utilizarla, tendrán una ventaja sobre el resto. Si a esto sumamos que los recursos energéticos necesarios para su desarrollo contribuyen al deterioro de nuestro medio ambiente, me parece que la inteligencia artificial, además de ser una respuesta, es un problema antropológico y una expresión más de la distribución desigual en la extracción de plusvalía.

Como carezco de los recursos, los conocimientos técnicos y el interés por computar mis textos al modo _deus ex machina_, mi aproximación para la computabilidad de los textos es mucho más reducida, ambigua y estéril. Mi interés es darte las tripas de mi discurso, su aparato lógico y óntico, en lugar de revelarte la respuesta para computar todos los textos posibles. De esta manera es que me veo en la necesidad de declarar las tablas de verdad, las proposiciones y su gramática.

Esta es una labor humana en la que, al mismo tiempo que te escribo, soy asistemáticamente consciente de la lógica y ontología que utilizó. Basta con declarar mis proposiciones como metadatos para después implementarlas en mi discurso. Si observas los [metadatos](https://pad.programando.li/zhenya:tablas-verdad?edit) de este texto, te darás cuenta que declaré un dato como $O$. Adentro de esta $O$ hay varias oraciones que uso en este texto. Por ejemplo, tengo:

```yaml x
O:
  atlanta_gana: 
    str: Atlanta gana el campeonato de su división
    val: true
  baltimore_gana:
    str: Baltimore gana el campeonato de su división
    val: true
  chicago_gana:
    str: Chicago gana el Supertazón
    val: true
  dallas_gana1:
    str: Dallas gana el Supertazón
    val: true
  dallas_gana2:
    str: Dallas gana el Supertazón
    val: false
```

Estas proposciones las usa Copi para sus primeros ejercicios y más adelante las uso para hacer pruebas de computación. Cada proposición dentro de $O$ cuenta con un valor y una llave, por ejemplo, `atlanta_gana`. El valor de cada proposición a su vez se compone de dos elementos. Con `str` declaro la cadena de caracteres (_string_ en inglés) de cada proposición; es decir, la oración que tú lees. Con `val` declaro su valor de verdad para ser procesado por una máquina.

Las últimas dos proposiciones de este ejemplo permiten hacer una distinción final. Te he dicho que un conjunto ordenado de proposiciones hacen un argumento y que según su disposción estas son premisas ó conclusiones. Pero en cuanto a las oraciones y proposiciones solo te había mencionado que las proposiciones escritas son oraciones. Con `dallas_gana1` y `dallas_gana2` observamos que, pese a ser la misma oración legible para nosotros, consisten en dos proposiciones distintas. La primera es una proposición «verdadera»; la segunda, «falsa». Entonces, es posible distinguir una proposición de una oración en el sentido de que existen casos donde una oración tiene un valor de verdad distinto según su disposición.

¿Cómo es posible que una oración tenga dos valores de verdad y, por ello, ser dos proposiciones que la hacen ser verdadera y falsa? Esta pregunta no es para la lógica o para los intereses de este texto. Recuerda, no es la lógica la que indica lo «verdadero» o lo «falso» de las cosas, sino la «validez» o «invalidez» de sus relaciones. A las otras disciplinas, como la psicología, las ciencias, la filosofía, la teología, las artes o la literatura, les corresponde defender o refutar la verdad o falsedad de tus proposiciones.

### Declaración de la gramática

Con la declaración de las proposiciones tengo varias de las cosas que empleo para mi discurso. Además, la declaración de las tablas de verdad me permiten formalizar las reglas para las relaciones válidas o inválidas entre mis proposiciones. No obstante, hasta ahora ambas declaraciones se encuentran inconexas. Sin una gramática, las cosas y sus relaciones permanecen en círculos distintos y equidistantes. Con una grámatica es posible sobreponer estos círculos para tener un área de intercambio de información, que en mi caso me permite computar las proposiciones de este texto.

<figure>
    
![](https://upload.wikimedia.org/wikipedia/commons/3/36/Diagrama_de_Venn_4b.png)

  <figcaption>

Figura 1. La intersección entre $A$ y $B$ es el área en donde podemos emplear una gramática con $A \cdot B$.

  </figcaption>
</figure>

Aunque por «gramática» me he permitido ciertas licencias filosóficas y hasta poéticas que a más de un computólogo le parecerán altos vuelos dignos de Ícaro, también con este término hago referencia a un uso más preciso. Por «gramática» también aludo a una [gramática libre de contexto](https://es.wikipedia.org/wiki/Gram%C3%A1tica_libre_de_contexto) que me permite tres cuestiones:

1. Con esta gramática implemento mis proposiciones en este texto con sintaxis $\TeX$ como $proposición1 \lor proposición2$.
2. En el [análisis sintáctico](https://es.wikipedia.org/wiki/An%C3%A1lisis_sint%C3%A1ctico_(ling%C3%BC%C3%ADstica)) de esta gramática la sintaxis $\TeX$ es reemplazada por los `str` de cada proposición, lo que te permitirá leerlas como si se tratasen de más palabras en mi discurso.
3. En ese mismo análisis dicha sintaxis es traducida a sintaxis de [Ruby](https://es.wikipedia.org/wiki/Ruby) para la computación y verificación de los `val` de cada proposición, y con ello, mostrarte si mis argumentos son válidos o no.

Es decir, con la declaración de esta gramática escribo como un lógico, leo como un filósofo y computo como un científico. Sin embargo, hay una cuestión importante que tengo que contarte respecto a semejante pretensión.

En las ciencias computacionales las gramáticas libres de contexto fueron la formalización del esfuerzo por disminuir la curva de aprendizaje para programar una computadora basada en silicio. Mientras que Chomsky y Schützenberger trabajaban la jerarquización de gramáticas formales, los científicos de la computación se esmeraban en hacer la computación más accesible. Fue a través de una serie de coincidencias por las que las investigaciones de Chomsky y Schützenberger pudieron trasladarse a las ciencias de la computación y, por ello, que ambos personajes sean estimados entre las comunidades de programadores y computólogos.

La computación basada en silicio emerge después de la Segunda Guerra Mundial a partir de las máquinas análogas que computaban papel perforado. Este papel representaba un programa compuesto por ceros y unos: el código máquina. El problema es que la programación directa de este código es una labor que pocas personas, como Turing, tienen la capacidad de realizar. Para el resto de los mortales esta manera de programar nos es sencillamente imposible porque nuestra escolarización tiene fama por sus deficiencias en las ciencias y las humanidades: desconocemos cómo formalizar aquello que sentimos y pensamos.

Poco a poco se han probado aproximaciones más amenas para programar. Así nacieron los lenguajes de programación. Estos lenguajes, como lo menciona Knuth, fueron pensados para ser escritos y leídos por homininos ---para mí es claro que aún hay un largo camino por recorrer: ¿cuántas personas tienen la capacidad escribir y leer con lenguajes de programación?---. Mediante su análisis sintáctico estos lenguajes se traducen a código máquina para su compilación o ejecución. Esta [capa de abstracción](https://es.wikipedia.org/wiki/Capa_de_abstracci%C3%B3n) en sus inicios se consideró innecesaria y hasta fue recibida con desdén. Con los años se demostró su pertinencia didáctica. Por ejemplo, los lenguajes de programación han permitido que miles de personas en la actualidad se dediquen a la maquila de código necesaria para que disfrutes de tus redes sociales, fotos, pelis, música, _ebooks_ o videojuegos.

Para este texto no requiero declarar una gramática para darte oraciones legibles y a la máquina, código para su ejecución. Basta con que programe una serie de reemplazos entre la sintaxis $\TeX$, la de Ruby y la del español. ¿Para qué, entonces, me empeñaría en declarar una gramática?

Por un lado, así te doy una muestra donde la «gramática» no es un artificio retórico o un aburrido y fracasado conjunto de reglas para regular el uso de una lengua. Para que mis pensamientos te sean inteligibles requiero disponerlos de cierta manera que sea coherente cuando los leas. Esta coherencia me es posible formalizarla en una gramática que me permite demostrar la validez de mis argumentos. Se trata de una producción de mi pensar, como mis palabras son una expresión de mi buen sentido.

Por otro lado, con esta gramática me dejo una nota de cómo podría ser un lenguaje de programación proposicional. No creo que este lenguaje llegue a ser adoptado, ¿cuántas personas ven la necesidad de explicitar sus proposiciones y, entre ellas, cuántas asienten en la pertinencia de su computabilidad? Sin embargo, para mis intereses son un gesto de cómo podría lucir un discurso escrito para ser computado. Se trata de un experimento en torno al diseño de un lenguaje formal que de manera sintética me permite escribir con coherencia mientras que para ti se «compila» según las reglas del español, al mismo tiempo que para la máquina es código ejecutable para la verificación de mi cordura. Así que toma en cuenta esta pretensión: no tienes por qué escribir así, no hay necesidad de llevar a cabo este procedimiento, es solo una manifestación de mi deseo por escribir ensayos computables.

Dicho esto, continuo con la manera formal en como se define una gramática libre de contexto: $G = (T, N, P, S)$. Es decir, una gramática libre de contexto $G$ es una secuencia de símbolos terminales $T$, símbolos no terminales $N$, sus reglas de producción $P$ y su símbolo inicial $S$. Los símbolos terminales $T$ son aquellos en donde la generación de una gramática se detiene. Los no terminales $N$ son símbolos que llaman a la continuidad de la generación. Las reglas de producción $P$ indican cómo es posible relacionar los símbolos $T$ y $N$, así como el símbolo inicial $S$ indica el punto de partida para $P$. Esto te puede sonar un tanto confuso, pero permíteme continuar bajo el supuesto que poco a poco te parecerá más claro.

Para la declaración de la gramática que requiero para relacionar mis proposiciones con las tablas de verdad de nuevo me valgo de los [metadatos](https://pad.programando.li/zhenya:tablas-verdad?edit). Ahí puedes encontrar lo siguiente:

```yaml x
G:
  T: true, false, \sim, \cdot, \lor, \oplus, \implies, \iff, (, )
  N: INI, VAL, LOG
  P:
    INI: VAL || LOG
    VAL: true || false
    LOG: ( \sim INI ) || ( INI \cdot INI ) || ( INI \lor INI ) || ( INI \oplus INI ) || ( INI \implies INI ) || ( INI \iff INI )
  S: INI
```

En esta gramática los terminales $T$ son los valores de verdad de mis proposiciones `true` o `false`, los paréntesis que me permiten aislar los elementos ---si observas las tablas de verdad que declaré, solo hay dos elementos: $p$ y $q$---, así como los conectores lógicos para relacionar los valores:

* `\sim` es $\sim$ en $\TeX$
* `\cdot`, $\cdot$
* `\lor`, $\lor$
* `\oplus`, $\oplus$
* `\implies`, $\implies$
* `\iff`, $\iff$

Los símbolos no terminales $N$ funcionan como variables. Por ejemplo, `INI` puede ser `VAL` ó `LOG` según el contexto, y `VAL`, los terminales `true` o `false`. En esta gramática el no terminal más complejo es `LOG`, esto se debe a que son todas las combinaciones lógicas permitidas: negación, conjunción, disyunción inclusiva, disyunción exclusiva, condición material y bicondición, respectivamente.

Por último, para indicar el inicio de la generación de esta gramática, con el símbolo inicial $S$ indico que la gramática comience con `INI`. Para que quede un poco más inteligible esta generación, a partir de diagramas a continuación ilustro la generación de la lógica proposicional.

#### Gráfica de identidad

$true$:

```graphviz x
digraph S {
  true[shape=box]
  INI -> VAL -> true
}
```

En estos diagramas los $T$ están representados como cajas en donde termina la generación y los $N$, como óvalos en donde la generación continua.

#### Gráfica de negación

$\sim true$:

```graphviz x
digraph S {
  true[shape=box]
  no[shape=box]
  INI1[label="INI"]
  INI -> LOG -> {no, INI1}
  INI1 -> VAL -> true
}
```

#### Gráfica de conjunción

$true \cdot false$:

```graphviz x
digraph S {
  true[shape=box]
  false[shape=box]
  a[label="INI"]
  b[shape=box, label="y"]
  c[label="INI"]
  VAL1[label="VAL"]
  VAL2[label="VAL"]
  INI -> LOG -> {a, b, c}
  a -> VAL1 -> true
  c -> VAL2 -> false
}
```

#### Gráfica de disyunción inclusiva

$true \lor false$:

```graphviz x
digraph S {
  true[shape=box]
  false[shape=box]
  a[label="INI"]
  b[shape=box, label="o"]
  c[label="INI"]
  VAL1[label="VAL"]
  VAL2[label="VAL"]
  INI -> LOG -> {a, b, c}
  a -> VAL1 -> true
  c -> VAL2 -> false
}
```

#### Gráfica de disyunción exclusiva

$true \oplus false$:

```graphviz x
digraph S {
  true[shape=box]
  false[shape=box]
  a[label="INI"]
  b[shape=box, label="ó"]
  c[label="INI"]
  VAL1[label="VAL"]
  VAL2[label="VAL"]
  INI -> LOG -> {a, b, c}
  a -> VAL1 -> true
  c -> VAL2 -> false
}
```

#### Gráfica de condición material

$true \implies false$:

```graphviz x
digraph S {
  true[shape=box]
  false[shape=box]
  a[label="INI"]
  b[shape=box, label="entonces"]
  c[label="INI"]
  VAL1[label="VAL"]
  VAL2[label="VAL"]
  INI -> LOG -> {a, b, c}
  a -> VAL1 -> true
  c -> VAL2 -> false
}
```

#### Gráfica de bicondición

$true \iff false$:

```graphviz x
digraph S {
  true[shape=box]
  false[shape=box]
  a[label="INI"]
  b[shape=box, label="solo si"]
  c[label="INI"]
  VAL1[label="VAL"]
  VAL2[label="VAL"]
  INI -> LOG -> {a, b, c}
  a -> VAL1 -> true
  c -> VAL2 -> false
}
```

### Programación de la gramática

Con la gramática declarada ya tengo todo lo necesario para empezar a programar :D

Te comenté hace varias secciones pasadas que el lenguaje de programación de mi preferencia es [Ruby](https://es.wikipedia.org/wiki/Ruby), pero no te dije cómo es que iba a usarlo para que este texto sea computable.

También te platiqué al principio que, para Knuth, la programación literaria es la «cosa más importante que salió de $\TeX$», y, para mí, un camino para probar maneras de escribir ensayos computables. Sin embargo, esto no quiere decir que se trata de la misma implementación de programación literaria.

La implementación de Knuth fue WEB y después CWEB. En la primera usaba [Pascal](https://es.wikipedia.org/wiki/Pascal_(lenguaje_de_programaci%C3%B3n)) para el texto computable y $\TeX$ para el texto legible por humanos. En la segunda cambió Pascal por [C](https://es.wikipedia.org/wiki/C_(lenguaje_de_programaci%C3%B3n)). Por mi parte, elaboré una implementación piloto con Ruby que se llama [RuWEB](https://pad.programando.li/ruweb). Mi implementación puede ejecutar textos computables en cualquier lenguaje que se le declare con sintaxis [Markdown](https://es.wikipedia.org/wiki/Markdown). Para su legibilidad me valgo de [HedgeDoc](https://hedgedoc.org), una plataforma _web_ de escritura colaborativa con Markdown.

Como se trata de dos implementaciones distintas, en RuWEB los macros se declaran e implementan de manera distinta a como se lleva a cabo en CWEB. Aquí los macros se declaran al partir del nombramiento de bloques de código, como puedes observar en el [glosario](#Glosario-de-macros) de este texto. Los macros pueden implementarse de dos maneras: dentro de un bloque de código o de manera discreta con un `=>`.

Para no atiborrarte la vista con un montón de código que quizá te parezca ininteligible, en esta ocasión prefiero implementar los macros con discreción. Por ejemplo, así implementamos y ejecutamos este => primer ensayo.

### Pruebas de las gramática

Baltimore gana el campeonato de su división

Atlanta gana el campeonato de su división y Chicago gana el Supertazón.

Baltimore gana el campeonato de su división entonces Dallas gana el Supertazón.

Atlanta gana el campeonato de su división y Baltimore gana el campeonato de su división, y Dallas gana el Supertazón, y Chicago gana el Supertazón, ó Atlanta gana el campeonato de su división.

Atlanta gana el campeonato de su división y Baltimore gana el campeonato de su división, y Dallas gana el Supertazón, entonces Chicago gana el Supertazón, ó Atlanta gana el campeonato de su división.

Atlanta gana el campeonato de su división ó Baltimore gana el campeonato de su división, y Baltimore gana el campeonato de su división, y Dallas gana el Supertazón, entonces Chicago gana el Supertazón, ó Atlanta gana el campeonato de su división.

## Actividad: ¿literatura computable?

## Glosario de macros

### Primer ensayo

```ruby _primer_ensayo
_motor

Log.verify('_fuente')
```

### Motor

```ruby= _motor
module Log
  require 'ruweb'
  require 'yaml'

  def self.verify(file_name)
    puts "Iniciando análisis sintáctico..."
    metadata = Get.metadata(file_name)
    text = Get.text(file_name)
    grammar = Parse.grammar(metadata['G'])
    propositions = metadata['O']
    Verify.text(grammar, propositions, text)
  end

  module Get
    extend self

    def metadata(file_name)
      content(file_name, 'metadata')
    end

    def text(file_name)
      content(file_name)
    end

    def formulas(str)
      raw = Parse.to_tex(str).scan(/>\$.+?\$/).uniq
      for_humans = raw.map { |e| Parse.for_humans(e) }
      for_computers = raw.map { |e| Parse.for_computers(e) }
      {written: raw, read: for_humans, result: for_computers}
    end

    private

    def content(file_name, type = 'data')
      blocks = RuWEB::Read.init(file_name).split(/\n---/)
      if type == 'metadata'
        YAML.load(blocks.first)
      else
        blocks.join("\n---")
      end
    end
  end

  module Parse
    extend self

    def grammar(raw)
      raw.each do | key, val |
        case key
        when 'T', 'N'
          raw[key] = val.split(/\s*,\s*/).map { |e| to_tex(e) }
        when 'P'
          raw[key] = production_rules(val)
        else 
          raw[key] = val
        end
      end
    end

    def to_tex(str)
      str.gsub("\u0024no", '\sim')
         .gsub("\u0024y",'\cdot')
         .gsub("\u0024o", '\lor')
         .gsub("\u0024ó", '\oplus')
         .gsub("\u0024entonces", '\implies')
         .gsub("\u0024solo_si", '\iff')
    end

    def for_humans(str)
      str[2..-2]
         .gsub('\sim', 'no')
         .gsub('\cdot','y')
         .gsub('\lor', 'o')
         .gsub('\oplus', 'ó')
         .gsub('\implies', 'entonces')
         .gsub('\iff', 'solo si')
         .gsub('(', '')
         .gsub(')', ',')
         .gsub(/\s+,/, ',')
         .gsub(/\s{2,}/, ' ')
         .gsub(/,$/, '')
    end

    def for_computers(str)
      str[2..-2]
         .gsub(/^([^\(])/, ) { |e| "( #{e}"}
         .gsub(/([^\)])$/, ) { |e| "#{e} )"}
         .gsub('(', '( ')
         .gsub(')', ' )')
         .gsub(/\(\s+\S+\s+\)/, ) { |e| e.split[1]}
         .split
    end

    private

    def production_rules(val)
      val.each do | key, v |
        val[key] = v.split(/\s*\|\|\s*/).map do |e|
          e = to_tex(e)
          key == 'LOG' ? e.split : e
        end
      end
    end

  end

  module Verify
    extend self

    def text(grammar, propositions, text)
      @grammar = grammar
      @propositions = propositions
      formulas = Get.formulas(text)
      formulas[:read] = check_str(formulas[:read])
      formulas[:result] = check_exec(formulas)
      print_results(formulas, text)
    end

    private

    def check_str(formulas)
      formulas.map do | formula |
        change_str(formula)
      end
    end

    def change_str(str)
      @propositions.each do | key, val |
        str.gsub!(key, val['str'])
      end
      str
    end

    def check_exec(formulas)
      puts "Fórmulas encontradas:"
      formulas[:result].map.with_index do | formula, i |
        checked = check(formulas[:written][i], formula, i + 1)
        checked = checked.join(' ').gsub('\\sim', 'not').gsub('\\cdot', 'and').gsub('\\lor', 'or')
        checked = reduce(checked)
        checked = replace(checked)
        replace(checked, /\S+\s+.+/)
      end
    end

    def check(original, formula, curr_num)
      @index = 0
      puts "#{curr_num}. #{original}"
      formula.map.with_index do | token, i |
        token = check_proposition(token, i) if val?(token)
        get_index(token)
        token
      end
    end

    def get_index(token)
      case token
      when '(' then @index = 1
      when ')' then @index = 2
      else @index = @index + 1
      end
    end

    def val?(token)
      res = false
      @grammar['P']['LOG'].each do | rule |
        @index = 0 if token == '(' && rule[@index] == 'INI'
        res = true if rule[@index] == token
      end
      !res
    end

    def reduce(str)
      pair = /\([^(\u005c|\()]+?\)/
      str = str.gsub(pair, ) do | match |
        "#{eval(match)}"
      end
      str =~ pair ? reduce(str) : str
    end

    def replace(str, pair = /\([^\(]+?\)/)
      str = str.gsub(pair, ) do | match |
        tokens = match.gsub(/(\(|\))/, '').strip.split
        refactor(tokens[0], tokens[1], tokens[2])
      end
      str =~ pair ? replace(str) : str
    end

    def refactor(p, operator, q)
      p = p == 'true' ? true : false
      q = q == 'true' ? true : false
      case operator
      when 'and' then _conjunción
      when 'or' then _disyunción_inclusiva
      when '\oplus' then _disyunción_exclusiva
      when '\implies' then _condición_material
      when '\iff' then _bicondición
      end
    end

    def check_proposition(token, i)
      if @propositions[token]
        if @propositions[token]['val'].nil?
          raise "'val' no encontrado para '#{token}'"
        else
          @propositions[token]['val'].to_s
        end
      else
        raise "sintaxis inválida en '#{i}:#{token}'"
      end
    end

    def print_results(formulas, text)
      puts "Imprimiendo resultados:"
      formulas[:result].each_with_index do | result, i |
        puts "#{i+1}. #{result}"
      end
      puts "Guardando texto para lectura como '_fuente.verified'."
      formulas[:written].each_with_index do | written, i |
        written = written
          .gsub('\\sim', "\u0024no")
          .gsub('\\cdot', "\u0024y")
          .gsub('\\lor', "\u0024o")
          .gsub('\\oplus', "\u0024ó")
          .gsub('\\implies', "\u0024entonces")
          .gsub('\\iff', "\u0024solo_si")
        text = text.gsub(written, formulas[:read][i])
      end
      File.write('_fuente.verified', text)
    end

  end

end
```

### Archivo fuente

```ruby _fuente
tablas_verdad.markdown
```

 <style>
  .alert {
    position: relative;
    overflow: hidden;
  }
  .alert .btn {
    border: 1px solid #31708f;
    color: #31708f;
    position: absolute;
    right: 0;
    bottom: 0;
    margin: 1em;
  }
  .bio {
    clear: both;
  }
  .bio div {
    float: left;
    width: 66%;
    margin-bottom: 2em;
  }
  .bio div:first-child {
    width: 33%;
    padding-right: 1em;
  }
  .bio img {
    max-height: 150px;
    display: block;
    margin: 0 auto;
  }
</style>